package soaint_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MoneyParts {   

    public static double build(int _i, double resto, String combinaciones) {
        double[] denominaciones = {0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10,20, 50, 100, 200};

		if (resto == 0) {
		    combinaciones = combinaciones.replaceAll(",$", "");
		    List<String> lst_final = new ArrayList<String>(Arrays.asList(combinaciones.split(",")));
			System.out.println(lst_final);
			return 1;
		}
		if (resto < 0) {
			return 0;
		}
 
		double res = 0.0;
		for (int i = _i; i < denominaciones.length; i++) {
			res += build(i, formatearDecimales(resto - denominaciones[i], 2), combinaciones + String.valueOf(denominaciones[i]) + ",");
		}
		return res; 
    }
    
    public static double formatearDecimales(double numero, Integer numeroDecimales) {
        return Math.round(numero * Math.pow(10, numeroDecimales)) / Math.pow(10, numeroDecimales);
    }
    
}